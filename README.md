(Ultimate) Tic Tac Toe Bot
==========================

Synopsis
--------

A bot which plays [Ultimate Tic Tac Toe](http://theaigames.com/competitions/ultimate-tic-tac-toe). See it playing games [here](http://theaigames.com/competitions/ultimate-tic-tac-toe/game-log/GeneAlgBot/1)


Method
------
A bot makes decisions via a neural network which takes in a board and macroboard as inputs, and outputs utility for each position in the board. The legal position with the greatest utility is chosen for each move.
A genetic algorithm grows a population of bots. The bots play against each other and are sorted by wins. The top 5% of bots are kept and mutated for the next generation.
After some number of generations, the top bot is chosen as the champion. Its neural network is saved to ```smartest_NN.py```.


Usage
-----

Evolve a population of bots
```
python2 brainbot/GA.py
```

Evaluate the performance of the best bot
```
python2 brainbot/evalulate_brainbot.py
```

To prepare the bot for submission, run the zipping script in the bot's directory or zip and submit the files:
* brainbot/main.py
* brainbot/position.py
* brainbot/mybot.py
* brainbot/NN.py
* brainbot/smartest_NN.py
