import os
import sys
from subprocess import Popen, PIPE, STDOUT

default_bot1 = 1 #1 = randombot
default_bot2 = 0 #0 = ga_bot
choose_bot_id_automatically = False
should_print_board = False
run_game_automatically = True

def main():
    # Get robots who are fighting (player1, player2)
    bot1, bot2 = get_bots()
    # Simulate game init input
    send_init('1', bot1)
    send_init('2', bot2)
    round_num = 1
    move = 1
    field = ','.join(['0'] * 81)
    macroboard = ','.join(['-1'] * 9)
    if should_print_board:
        print_board(field, macroboard, round_num, '')
    while True:
        for bot_id, bot in [('1', bot1), ('2', bot2)]:
            # Wait for any key
            if not run_game_automatically:
                raw_input()
            # Send inputs to bot
            move = send_update(bot, round_num, move, field, macroboard)
            # Update macroboard and game field
            field = update_field(field, move, str(bot_id))
            macroboard = update_macroboard(field, move)
            # Print the board state
            if should_print_board:
                print_board(field, macroboard, round_num, move)
            # Check for winner. If winner, exit.
            (somebotty_won, winning_bot_num) = get_winner(macroboard)
            if somebotty_won:
                if winning_bot_num == 0:
                    print("It was a tie")
                elif winning_bot_num == 1:
                    print("Bot1 wins")
                elif winning_bot_num == 2:
                    print("Bot2 wins")
                return

            round_num += 1


def get_bots():
    root = os.path.dirname(os.path.realpath(__file__))
    files = os.listdir(root)
    bots = [f for f in files
            if os.path.isdir(os.path.join(root, f)) and f != '.git']

    bot_list = '\n'.join(
        ['{}. {}'.format(i, bot) for i, bot in enumerate(bots)])

    if not choose_bot_id_automatically:
        bot1_name = bots[int(raw_input(
            'Choose Player 1:\n' + bot_list + '\n\n> '))]
        bot2_name = bots[int(raw_input(
            'Choose Player 2:\n' + bot_list + '\n\n> '))]
    else:
        bot1_name = bots[default_bot1]
        bot2_name = bots[default_bot2]

    bot1 = Popen(['python', 'main.py'],
                 cwd=os.path.join(root, bot1_name),
                 stdout=PIPE,
                 stdin=PIPE,
                 stderr=STDOUT)
    bot2 = Popen(['python', 'main.py'],
                 cwd=os.path.join(root, bot2_name),
                 stdout=PIPE,
                 stdin=PIPE,
                 stderr=STDOUT)

    return bot1, bot2


def send_init(bot_id, bot):
    init_input = (
        'settings timebank 10000\n'
        'settings time_per_move 500\n'
        'settings player_names player1,player2\n'
        'settings your_bot player{bot_id}\n'
        'settings your_botid {bot_id}\n'.format(bot_id=bot_id))

    bot.stdin.write(init_input)


def send_update(bot, round_num, move, field, macroboard):
    update_input = (
        'update game round {round}\n'
        'update game move {move}\n'
        'update game field {field}\n'
        'update game macroboard {macro}\n'
        'action move 10000\n'.format(
            round=round_num,
            move=move,
            field=field,
            macro=macroboard))

    bot.stdin.write(update_input)
    out = bot.stdout.readline().strip()

    if all([not out.startswith(x) for x in ["settings","update","action","place_move"]]):
        print("~~~~~~~~~~~~~~~~~~~~~~~")
        toWrite = "owl"
        while not toWrite == "":
            toWrite = bot.stdout.readline().strip()
            sys.stdout.write(toWrite+"\n")
        print("~~~~~~~~~~~~~~~~~~~~~~~")
    if should_print_board:
        print 'bot output: ' + repr(out)
    return out


def update_field(field, move, bot_id):
    print("move: "+move)

    col, row = move.split(' ')[1:3]
    arr = field.split(',')
    index = int(row) * 9 + int(col)
    if arr[index] != '0':
        raise RuntimeError(
            'Square {col} {row} already occupied by {occ}.'.format(
                col=col, row=row, occ=arr[index]))

    arr[index] = bot_id
    return ','.join(arr)


def update_macroboard(field, move):
    # break it up into small boards
    board = field.split(',')
    small_boards = []
    for r in range(0, 9, 3):
        for c in range(0, 9, 3):
            sb = []
            sb.extend(board[r * 9 + c:r * 9 + c + 3])
            sb.extend(board[(r + 1) * 9 + c:(r + 1) * 9 + c + 3])
            sb.extend(board[(r + 2) * 9 + c:(r + 2) * 9 + c + 3])
            small_boards.append(sb)

    # determine macro board state
    def get_state(a):
        winopts = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [6, 4, 2]]

        winners = ('111', '222')
        for opt in winopts:
            val = a[opt[0]] + a[opt[1]] + a[opt[2]]
            if val in winners:
                return a[opt[0]]

        if '0' not in a:
            return '3'

        return '0'

    macroboard = [get_state(b) for b in small_boards]

    # modify macro board state based on availability of small board
    col, row = move.split(' ')[1:3]
    index = int(row) * 9 + int(col)
    boards = [
        [0, 3, 6, 27, 30, 33, 54, 57, 60],  # top-left
        [1, 4, 7, 28, 31, 34, 55, 58, 61],  # top-middle
        [2, 5, 8, 29, 32, 35, 56, 59, 62],  # top-right
        [9, 12, 15, 36, 39, 42, 63, 66, 69],  # middle-left
        [10, 13, 16, 37, 40, 43, 64, 67, 70],  # middle-middle
        [11, 14, 17, 38, 41, 44, 65, 68, 71],  # middle-right
        [18, 21, 24, 45, 48, 51, 72, 75, 78],  # bottom-left
        [19, 22, 25, 46, 49, 52, 73, 76, 79],  # bottom-middle
        [20, 23, 26, 47, 50, 53, 74, 77, 80]]  # bottom-right

    for i, b in enumerate(boards):
        if index in b:
            # If macro space available, update it to -1
            if macroboard[i] == '0':
                macroboard[i] = '-1'
                break
            else:  # If macro space not available, update all 0 to -1
                macroboard = ['-1' if m == '0' else m for m in macroboard]
                break

    return ','.join(macroboard)


def print_board(field, macroboard, round_num, move):
    field = field.replace('0', ' ')
    a = field.split(',')
    msg = ''
    for i in range(0, 81, 9):
        if not i % 27 and i > 0:
            msg += '---+---+---\n'

        msg += '|'.join([
            ''.join(a[i:i+3]),
            ''.join(a[i+3:i+6]),
            ''.join(a[i+6:i+9])]) + '\n'

    sys.stderr.write("\x1b[2J\x1b[H")  # clear screen
    msg += '\nRound {}\nmacroboard: {}\nfield: {}\nmove: {}\n'.format(
        round_num, macroboard, field, move)

    sys.stdout.write(msg)


def get_winner(macroboard):
    winopts = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [6, 4, 2]]

    m = macroboard.split(',')

    #if the game ends in a tie
    if not ('-1' in m or '0' in m):
        return (True, 0)

    winners = ('111', '222')
    for opt in winopts:
        val = m[opt[0]] + m[opt[1]] + m[opt[2]]
        if val in winners:
            # print 'WINNER! Player {}'.format()
            return (True, int(m[opt[0]]))

    return (False, None)


def is_winner(macroboard):
    return get_winner(macroboard)[0]


if __name__ == '__main__':
    main()
