from random import randint, choice
import numpy as np
from NN import NN

def smartest():
    # return from_file("smartest_NN.p")
    import smartest_NN as smartest
    return from_pickled_string(smartest.pickled_string)

def from_pickled_string(pickled_string):
    myNN = NN.load_from_pickled_string(pickled_string)
    bot = BrainBot(myNN)
    return bot

def from_file(filename):
    myNN = NN.load_from_file(filename)
    bot = BrainBot(myNN)
    return bot

class BrainBot:
    def __init__(self, myNN):
        self.myNN = myNN
        self.visualize_each_move = False

    def visualize(self, output):
        'print the array of utility'
        shaped = (output.reshape(9, 9) * 100).astype(int)
        print(shaped)

        'graph utility with matplotlib'
        # import matplotlib.pyplot as plt
        # import numpy as np
        # plt.figure(1)
        # plt.imshow(output.reshape(9, 9), interpolation='nearest')
        # plt.grid(True)
        # plt.show()

        pass


    def get_move(self, pos, tleft):

        'SMART MOVES'
        input_values = []
        input_values.extend(pos.board[:]) # the whole board
        input_values.extend(pos.macroboard[:]) # the macroboard

        output = self.myNN.runNN(input_values)

        if self.visualize_each_move:
            self.visualize(output)

        #attempt to move to the place in descending order of activation/happiness
        args = np.argsort(output)
        lmoves = pos.legal_moves()
        for num in args:
            x, y = num % 9, num / 9
            if (x, y) in lmoves:
                return (x, y)
        print("ERROR - THIS CODE SHOULD NOT BE REACHED")

        # rm = randint(0, len(lmoves)-1)
        # return choice(lmoves)


        'RANDOM LEGAL MOVES'
        # lmoves = pos.legal_moves()
        # rm = randint(0, len(lmoves)-1)
        # return lmoves[rm]
