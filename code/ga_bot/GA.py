from __future__ import print_function

import csv
import numpy as np
import time
from random import choice, random, uniform
np.set_printoptions(precision=5)

from NN import NN
from mybot import BrainBot
import eval_bot

class Params:
    # change to optimize generally (bigger is probably better)
    num_generations = 1
    pop_size = 10
    winner_percentage = 0.05 #1.0 = 100%
    num_winners = int(max(round(winner_percentage*pop_size), 1))

    # change to optimize generally
    mutation_probability = 0.8#0.2 #probability that a weight will be mutated
    mutation_initial_temp = 2.0
    mutation_cooling_rate = 0.93 #higher is slower

    # change when adding more features
    NN_inputs = 81+9
    NN_hidden_nodes_per_layer = 9
    NN_hidden_layers = 2

class GA:
    @staticmethod
    def create_random_member():
        myNN = NN(nI=Params.NN_inputs, nHperLayer=Params.NN_hidden_nodes_per_layer, nHlayers=Params.NN_hidden_layers, nO=81)
        return myNN


    @staticmethod
    def random_seeds(num_seeds):
        seeds = [GA.create_random_member() for _ in range(num_seeds)]
        return seeds


    def __init__(self, pop_size=100):
        self.pop = [GA.create_random_member() for _ in range(pop_size)]
        self.fitness_values = [0 for _ in range(pop_size)]
        self.generation_num = 0


    def mutate_rate(self):
        return Params.mutation_initial_temp * ((Params.mutation_cooling_rate ** self.generation_num))


    def mutate_member(self, member):
        weights = np.copy(member.get_weights())

        # weights = weights + self.mutate_rate()*np.random.normal(0, 1.0)
        random_deltas = self.mutate_rate()*np.random.normal(0, 1.0) #note that 1.0 is the std dev
        
        weights[np.random.rand(*weights.shape) < Params.mutation_probability] += random_deltas

        myNN = GA.create_random_member()
        myNN.set_weights(weights)
        return myNN


    def get_seeded_population(self, seeds, size=100):
        if len(seeds) == 0:
            seeds = [GA.create_random_member()]
        population = []
        population.extend(seeds)
        num_children = size-len(seeds)
        for x in range(num_children):
            seed_index = int(1.0*x/num_children)
            seed = seeds[seed_index]
            population.append(self.mutate_member(seed))
        return population


    def get_winner(self, member1, member2):
        # have them play a game against each other
        # return 0 if tie or member1 wins, 1 otherwise

        import my_server
        my_server.print_game_results = False

        bot1 = BrainBot(member1)
        bot2 = BrainBot(member2)
        winner = my_server.get_winner_of_1_game(bot1, bot2)

        return winner


    def get_top_winners(self, num_winners):
        assert(num_winners <= len(self.pop))

        def compare(member1, member2):
            winner_num = self.get_winner(member1, member2)
            if winner_num == 1: return -1
            elif winner_num == 2: return 1
            else: return 0
        
        members_sorted_by_wins = sorted(self.pop, cmp=compare)

        # members_sorted_by_wins = sorted(self.pop, key=lambda x: eval_bot.get_bot_fitness(BrainBot(x)))


        # win_counts = [0 for _ in range(len(self.pop))]
        # for i in range(len(self.pop)):
        #     for j in range(i):
        #         winner_num = self.get_winner(self.pop[i], self.pop[j])
        #         if winner_num == 1:
        #             win_counts[i] += 1
        #         elif winner_num == 2:
        #             win_counts[j] += 1
        # members_sorted_by_wins = sorted(zip(self.pop, win_counts), key=lambda x: -x[1])
        # members_sorted_by_wins, win_counts = zip(*members_sorted_by_wins)

        self.pop = members_sorted_by_wins
        # self.fitness_values = win_counts

        return members_sorted_by_wins[:num_winners]


    def evolve(self, num_winners=10):
        next_gen_seeds = self.get_top_winners(num_winners)
        self.pop = self.get_seeded_population(next_gen_seeds, len(self.pop))
        self.generation_num += 1


def run_experiment():
    load_seeds = False # load the seeds from the last time the program was run (experimental)
    seeds = []
    if load_seeds:
        for i in range(Params.num_winners):
            seeds.append(NN.load_from_file("seeds/NN_member_"+str(i)+".p"))
    else:
        seeds = GA.random_seeds(Params.num_winners)

    # print the current variables
    print("GA: {} generations, keeping {} / {}".format(Params.num_generations, Params.num_winners, Params.pop_size))
    print("Mutation: prob {}, initial {}, cooling {}".format(Params.mutation_probability, Params.mutation_initial_temp, Params.mutation_cooling_rate))
    print("NN: inputs {}, breadth {}, depth {}".format(Params.NN_inputs, Params.NN_hidden_nodes_per_layer, Params.NN_hidden_layers))

    print("   % gen lost won tied fit")
    myGA = GA(Params.pop_size)
    myGA.pop = myGA.get_seeded_population(seeds, Params.pop_size)
    for generation_num in range(Params.num_generations):
        myGA.evolve(Params.num_winners)

        # print("saving best network to file")
        # myGA.pop[0].save_to_file("smartest_NN.p")
        myGA.pop[0].save_to_python_file("smartest_NN.py")

        # print(str(100.0*generation_num/num_generations)+"""% complete - generation """+str(generation_num+1)+" of "+str(num_generations))
        # eval_bot.print_fitness()
        (losses, wins, ties) = eval_bot.get_games_stats()
        completion = 100.0*generation_num/Params.num_generations
        print("{:2.1f} {:3d} {:4d} {:3d} {:4d} {:3d}".format(completion, generation_num+1, losses, wins, ties, wins-losses))

        # print("saving top seeds")
        for i in range(Params.num_winners):
            myGA.pop[i].save_to_file("seeds/NN_member_"+str(i)+".p")


#grow multiple populations for each parameter
def science():
    # Try to find optimal mutation parameters
    print("\nTESTING mutation_probability")
    for x in np.arange(0.05, 1.05, 0.05):
        Params.mutation_probability = x
        run_experiment()
    Params.mutation_probability = 0.2

    print("\nTESTING mutation_initial_temp")
    for x in np.arange(0.1, 2.1, 0.1):
        Params.mutation_initial_temp = x
        run_experiment()
    Params.mutation_initial_temp = 1.0

    print("\nTESTING mutation_cooling_rate")
    for x in np.arange(0.90, 0.99, 0.01):
        Params.mutation_cooling_rate = x
        run_experiment()
    Params.mutation_cooling_rate = 0.95


    # Try to find optimal genetic algorithm parameters
    print("\nTESTING num_generations")
    for x in range(10, 210, 10):
        Params.num_generations = x
        run_experiment()
    Params.num_generations = 10

    print("\nTESTING pop_size")
    for x in range(10, 210, 10):
        Params.pop_size = x
        run_experiment()
    Params.pop_size = 10
    
    print("\nTESTING num_winners")
    Params.pop_size = 100
    for x in [1, 2, 3, 4, 5, 10, 15, 20, 25, 40, 50, 60, 70]:
        Params.num_winners = x
        run_experiment()
    Params.num_winners = 4
    Params.pop_size = 10

    print("\nTESTING NN_hidden_nodes_per_layer")
    for x in [1, 2, 4, 9, 18, 27, 36, 50, 60, 70, 81]:
        Params.NN_hidden_nodes_per_layer = x
        run_experiment()
    Params.NN_hidden_nodes_per_layer = 50

    print("\nTESTING NN_hidden_layers")
    for x in [1, 2, 3]:
        Params.NN_hidden_layers = x
        run_experiment()
    Params.NN_hidden_layers = 2


def main():
    # grow population using current parameters
    run_experiment()

    # grow many populations to find an optimal set of parameters
    # science()

    #DEBUG
    # print("\nTESTING NN_hidden_nodes_per_layer")
    # for x in [1, 2, 4, 9, 18, 27, 36, 50, 60, 70, 81]:
    #     Params.NN_hidden_nodes_per_layer = x
    #     run_experiment()
    # Params.NN_hidden_nodes_per_layer = 50

    # print("\nTESTING NN_hidden_layers")
    # for x in [1, 2, 3]:
    #     Params.NN_hidden_layers = x
    #     run_experiment()
    # Params.NN_hidden_layers = 1


if __name__ == "__main__":
    start_time = time.clock()
    main()
    print()
    seconds = (time.clock() - start_time)
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    d, h = divmod(h, 24)
    d, h, m = int(d), int(h), int(m)
    print("Runtime: {0}d {1}h {2}m {3:.3f}s".format(d, h, m, s))
    
    # import winsound
    # winsound.Beep(3000,1000)
    
    # import os
    # os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % ( 1, 3000.0))

