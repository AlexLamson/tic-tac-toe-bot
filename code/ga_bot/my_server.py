#!/usr/bin/env python

from position import Position
from mybot import BrainBot
from randombot import RandomBot


print_game_results = False # if true print who won
run_game_automatically = True # if true you don't have to have press a key to make moves
print_game = False # if true print the whole board every move



def get_stats_of_n_games(bot1, bot2, number_of_games=1):
    bot1_win_count, bot2_win_count, tie_count = 0, 0, 0

    for _ in range(number_of_games):
        winner_found = False
        pos1, pos2 = Position(), Position()
        round_num = 1
        move = 1
        field = ','.join(['0'] * 81)
        macroboard = ','.join(['-1'] * 9)
        if print_game:
            print_board(field, macroboard, round_num, '')

        for round_num in range(1, int(82/2)):
            for (bot, pos, bot_id) in [(bot1, pos1, 1), (bot2, pos2, 2)]:
                # Wait for any key
                if not run_game_automatically:
                    raw_input()

                #update board state
                pos.parse_field(field)
                pos.parse_macroboard(macroboard)

                move_col, move_row = bot.get_move(pos, 1000)
                pos.make_move(move_row, move_col, bot_id)

                field = update_field(field, move_col, move_row, str(bot_id))
                macroboard = update_macroboard(field, move_col, move_row)

                if print_game:
                    print_board(field, macroboard, round_num, move)

                #check if there is a winner
                (somebotty_won, winning_bot_num) = get_winner(macroboard)

                if somebotty_won:
                    if winning_bot_num == 0:
                        tie_count += 1
                        if print_game_results:
                            print("It twas a tie")
                    elif winning_bot_num == 1:
                        bot1_win_count += 1
                        if print_game_results:
                            print("bot {} won".format(winning_bot_num))
                    elif winning_bot_num == 2:
                        bot2_win_count += 1
                        if print_game_results:
                            print("bot {} won".format(winning_bot_num))
                    winner_found = True
                    break
            if winner_found:
                break

    if number_of_games > 1 and print_game_results:
        print("bot1 won "+str(bot1_win_count)+" and bot2 won "+str(bot2_win_count)+" and there were "+str(tie_count)+" tie games")

    return (bot1_win_count, bot2_win_count, tie_count)


def get_winner_of_1_game(bot1, bot2):
    bot1_win_count, bot2_win_count, _ = get_stats_of_n_games(bot1, bot2, number_of_games=1)
    if bot1_win_count == bot2_win_count:
        return 0
    elif bot1_win_count > bot2_win_count:
        return 1
    else:
        return 2




def update_field(field, col, row, bot_id):
    arr = field.split(',')

    index = int(row) * 9 + int(col)
    if arr[index] != '0':
        raise RuntimeError(
            'Square {col} {row} already occupied by {occ}.'.format(
                col=col, row=row, occ=arr[index]))

    arr[index] = bot_id
    return ','.join(arr)


def update_macroboard(field, col, row):
    # break it up into small boards
    board = field.split(',')
    small_boards = []
    for r in range(0, 9, 3):
        for c in range(0, 9, 3):
            sb = []
            sb.extend(board[r * 9 + c:r * 9 + c + 3])
            sb.extend(board[(r + 1) * 9 + c:(r + 1) * 9 + c + 3])
            sb.extend(board[(r + 2) * 9 + c:(r + 2) * 9 + c + 3])
            small_boards.append(sb)

    # determine macro board state
    def get_state(a):
        winopts = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [6, 4, 2]]

        winners = ('111', '222')
        for opt in winopts:
            val = a[opt[0]] + a[opt[1]] + a[opt[2]]
            if val in winners:
                return a[opt[0]]

        if '0' not in a:
            return '3'

        return '0'

    macroboard = [get_state(b) for b in small_boards]

    # modify macro board state based on availability of small board
    # col, row = move.split(' ')[1:3]
    index = int(row) * 9 + int(col)
    boards = [
        [0, 3, 6, 27, 30, 33, 54, 57, 60],  # top-left
        [1, 4, 7, 28, 31, 34, 55, 58, 61],  # top-middle
        [2, 5, 8, 29, 32, 35, 56, 59, 62],  # top-right
        [9, 12, 15, 36, 39, 42, 63, 66, 69],  # middle-left
        [10, 13, 16, 37, 40, 43, 64, 67, 70],  # middle-middle
        [11, 14, 17, 38, 41, 44, 65, 68, 71],  # middle-right
        [18, 21, 24, 45, 48, 51, 72, 75, 78],  # bottom-left
        [19, 22, 25, 46, 49, 52, 73, 76, 79],  # bottom-middle
        [20, 23, 26, 47, 50, 53, 74, 77, 80]]  # bottom-right

    for i, b in enumerate(boards):
        if index in b:
            # If macro space available, update it to -1
            if macroboard[i] == '0':
                macroboard[i] = '-1'
                break
            else:  # If macro space not available, update all 0 to -1
                macroboard = ['-1' if m == '0' else m for m in macroboard]
                break

    return ','.join(macroboard)

def print_board(field, macroboard, round_num, move):
    field = field.replace('0', ' ')
    a = field.split(',')
    msg = ''
    for i in range(0, 81, 9):
        if not i % 27 and i > 0:
            msg += '---+---+---\n'

        msg += '|'.join([
            ''.join(a[i:i+3]),
            ''.join(a[i+3:i+6]),
            ''.join(a[i+6:i+9])]) + '\n'
    print(msg)


def get_winner(macroboard):
    winopts = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [6, 4, 2]]

    m = macroboard.split(',')

    #if the game ends in a tie
    if not ('-1' in m or '0' in m):
        return (True, 0)

    winners = ('111', '222')
    for opt in winopts:
        val = m[opt[0]] + m[opt[1]] + m[opt[2]]
        if val in winners:
            # print 'WINNER! Player {}'.format()
            return (True, int(m[opt[0]]))

    return (False, None)


def is_winner(macroboard):
    return get_winner(macroboard)[0]


def visualize_game():
    import mybot

    bot1 = RandomBot()
    bot2 = mybot.smartest()
    bot2.visualize_each_move = True
    winner_num = get_winner_of_1_game(bot1, bot2)
    print("bot "+str(winner_num)+" wins")


if __name__ == '__main__':
    # visualize_game()

    import mybot
    bot1 = RandomBot()
    bot2 = mybot.smartest()
    winner_num = get_winner_of_1_game(bot1, bot2)
    print("the winner is bot", winner_num)

