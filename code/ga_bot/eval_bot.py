import mybot
from randombot import RandomBot
import my_server as server

def get_bot_fitness(bot, games=1):
    bot1 = RandomBot()
    bot2 = bot

    server.print_game_results = False
    losses, wins, ties = server.get_stats_of_n_games(bot1, bot2, number_of_games=games)
    return wins-losses

def get_games_stats(num_games=100):
    bot1 = RandomBot()
    bot2 = mybot.smartest()

    server.print_game_results = False
    return server.get_stats_of_n_games(bot1, bot2, number_of_games=num_games)

def print_fitness(num_games=100):
    (losses, wins, ties) = get_games_stats(num_games)
    print("losses: {}  wins: {}  ties: {}".format(losses, wins, ties))


if __name__ == '__main__':
    print_fitness(1000)
