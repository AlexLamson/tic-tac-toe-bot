from __future__ import print_function

import pickle
import csv
import numpy as np
import time
np.set_printoptions(precision=5)


def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))

def sigmoid_prime(x):
    s = sigmoid(x)
    return s*(1.0-s)

# hyperbolic tangent is often better than sigmoid, and you're welcome to try it, or others
def tanh(x):
    return np.tanh(x)

def tanh_prime(x):
    return 1.0 - tanh(x)**2

def activation_function(x):
    return sigmoid(x)
#    return tanh(x)

def activation_prime(x):
    return sigmoid_prime(x)
#    return tanh_prime(x)


class NN:
    def __init__(self, nI=256, nHperLayer=128, nHlayers=1, nO=10):
        # Randomly initialize our network weights with mean 0
        # This implicitly initializes nodes, which become explicit in runNN
        # first Input layer (nI) has 3 inputs, second Hidden (nH) has 4 neurons

        #each element is a 2d array of the synapses between that layer and the next
        self.synList = [None]*(1+nHlayers)
        self.synList[0] = 2 * np.random.random((nI, nHperLayer)) - 1
        
        for i in range(nHlayers-1):
            self.synList[1+i] = 2 * np.random.random((nHperLayer, nHperLayer)) - 1
        
        self.synList[-1] = 2 * np.random.random((nHperLayer, nO)) - 1
        
        self.layerList = [None]*(1+nHlayers+1)

    def runNN(self, X):
        
        self.layerList[0] = X
        
        for i in range(1, len(self.layerList)):
            self.layerList[i] = activation_function(np.dot(self.layerList[i-1], self.synList[i-1]))
        
        return self.layerList[-1]


    def get_weights(self):
        return self.synList

    def set_weights(self, synList):
        self.synList = synList

    # Usage: myNN.save_to_python_file("snoo.pp")
    def save_to_python_file(self, filename_str):
        with open(filename_str, 'wb') as handle:
            pickled_string = pickle.dumps(self).encode('base64')
            handle.write("pickled_string = '''"+pickled_string+"'''")

    # Usage: myNN.save_to_file("snoo.p")
    def save_to_file(self, filename_str):
        with open(filename_str, 'wb') as handle:
            handle.write(pickle.dumps(self).encode('base64'))

    # Usage: NN.load_from_file("snoo.p")
    @staticmethod
    def load_from_file(filename_str):
        with open(filename_str, 'rb') as handle:
            myNN = pickle.loads(handle.read().decode('base64'))
            return myNN
        print("THERE WAS AN ERROR LOADING THE NETWORK")
        return None


    # Usage: NN.loadpickled_string(my_pickled_string)
    @staticmethod
    def load_from_pickled_string(pickled_string):
        myNN = pickle.loads(pickled_string.decode('base64'))
        return myNN

    # Usage: NN.load_from_file("snoo.p")
    @staticmethod
    def load_from_weights_string(weights_string):
        myNN = NN(nI=81, nHperLayer=50, nHlayers=2, nO=81)
        myNN.set_weights(np.fromstring(weights_string))
        return myNN
